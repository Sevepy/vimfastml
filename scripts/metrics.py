#!/usr/bin/env python
import sys
from sklearn import metrics
import numpy as np
ypred = np.loadtxt(f"{sys.argv[1]}/labels_pred.csv", dtype=int)
ytest = np.loadtxt(f"{sys.argv[1]}/labels_test.csv", dtype=int)
print(metrics.classification_report(ypred, ytest))
