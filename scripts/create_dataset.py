#!/usr/bin/env python
import pandas as pd
import sys
from sklearn.datasets import make_blobs
X, y = make_blobs(n_samples=300, centers=5 ,random_state=0, center_box=(-1,1)
        ,cluster_std=0.2)

# features matrix 
fm = pd.DataFrame(data = X)

# labels vector
lv = pd.DataFrame(data = y)

df = pd.concat([fm,lv],axis=1)
try:
    dataset=f"{sys.argv[1]}/dataset.csv"
except IndexError:
    dataset="./data/dataset.csv"

df.to_csv(dataset,header=False,index=False)
