
.PHONY: decisiontree randomforest clean

decisiontree:
	make -f MakeDecisionTree

randomforest:
	make -f MakeRandomForest

clean:
	rm ./data/*.csv

